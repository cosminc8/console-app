# console-app

basic Java console app that mimics an article writing blog 
using:
- Java SE (Maven project)
- MySQL
- Hibernate
- MVC architectural pattern
