package model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true, length = 35)
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "user_description")
    private String userDescription;

    @OneToMany(mappedBy = "user")
    private Set<Article> articles = new HashSet<>();

    @OneToMany(mappedBy = "userFeedback")
    private Set<Feedback> feedbacks = new HashSet<>();

    public User() {
    }

    public User(String firstName, String lastName, String email, String password,
                String userDescription, Set<Article> articles, Set<Feedback> feedbacks) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.userDescription = userDescription;
        this.articles = articles;
        this.feedbacks = feedbacks;
    }

    public Set<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public User setFeedbacks(Set<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        return this;
    }

    public Set<Article> getArticles() {
        return articles;
    }

    public User setArticles(Set<Article> articles) {
        this.articles = articles;
        return this;
    }

    public long getId() {
        return id;
    }

    public User setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public User setUserDescription(String userDescription) {
        this.userDescription = userDescription;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userDescription='" + userDescription + '\'' +
                ", articles=" + articles +
                ", feedbacks=" + feedbacks +
                '}';
    }
}
