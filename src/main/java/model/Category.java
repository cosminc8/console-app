package model;

public enum Category {
    GENERAL,
    JAVA,
    HTML,
    JS,
    CSS
}
