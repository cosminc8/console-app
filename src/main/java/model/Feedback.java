package model;

import javax.persistence.*;

@Entity
@Table(name = "feedback")
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "comments")
    private String comments;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User userFeedback;

    @ManyToOne
    @JoinColumn(name = "articleId")
    private Article article;

    public Feedback() {
    }

    public Feedback(String comments, User userFeedback, Article article) {
        this.comments = comments;
        this.userFeedback = userFeedback;
        this.article = article;
    }

    public long getId() {
        return id;
    }

    public Feedback setId(long id) {
        this.id = id;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public Feedback setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public User getUserFeedback() {
        return userFeedback;
    }

    public Feedback setUserFeedback(User userFeedback) {
        this.userFeedback = userFeedback;
        return this;
    }

    public Article getArticle() {
        return article;
    }

    public Feedback setArticle(Article article) {
        this.article = article;
        return this;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", comments='" + comments + '\'' +
                ", userFeedback=" + userFeedback +
                ", article=" + article +
                '}';
    }
}
