package model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "article")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private Category category;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "published_date")
    private LocalDate publishedDate;

    @Column(name = "likes")
    private int likes;

    @Column(name = "dislikes")
    private int dislikes;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @OneToMany(mappedBy = "article")
    private Set<Feedback> feedbacks = new HashSet<>();

    public Article() {
    }

    public Article(Category category, String title, String content,
                   LocalDate publishedDate, int likes, int dislikes, User user,
                   Set<Feedback> feedbacks) {
        this.category = category;
        this.title = title;
        this.content = content;
        this.publishedDate = publishedDate;
        this.likes = likes;
        this.dislikes = dislikes;
        this.user = user;
        this.feedbacks = feedbacks;
    }

    public long getId() {
        return id;
    }

    public Article setId(long id) {
        this.id = id;
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public Article setCategory(Category category) {
        this.category = category;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Article setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Article setContent(String content) {
        this.content = content;
        return this;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public Article setPublishedDate(LocalDate publishedDate) {
        this.publishedDate = publishedDate;
        return this;
    }

    public int getLikes() {
        return likes;
    }

    public Article setLikes(int likes) {
        this.likes = likes;
        return this;
    }

    public int getDislikes() {
        return dislikes;
    }

    public Article setDislikes(int dislikes) {
        this.dislikes = dislikes;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Article setUser(User user) {
        this.user = user;
        return this;
    }

    public Set<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public Article setFeedbacks(Set<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        return this;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", category=" + category +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", publishedDate=" + publishedDate +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                ", user=" + user +
                ", feedbacks=" + feedbacks +
                '}';
    }
}
