import model.Article;
import model.User;
import service.ArticleService;
import service.FeedbackService;
import service.UserService;
import view.AppCommands;
import view.Menu;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        UserService userService = new UserService();
        ArticleService articleService = new ArticleService();
        FeedbackService feedbackService = new FeedbackService();

        Menu menu = new Menu();

        boolean validateUser = false;
        Article article;
        User user = null;
        long articleId = 0;

        AppCommands command;
        do {
            try {
                AppCommands.printOptions(validateUser);
                command = AppCommands.valueOf(SCANNER.nextLine());
            } catch (IllegalArgumentException e) {
                command = AppCommands.OPTION_NOT_DEFINED;
            }

            try {
                switch (command) {
                    case REGISTER_USER:
                        System.out.println(command.getCommandText());
                        //register new logic
                        User newUser = menu.showRegisterForm(SCANNER);
                        userService.registerUser(newUser);
                        break;
                    case LOGIN_USER:
                        System.out.println(command.getCommandText());
                        //login logic
                        List<String> credentials = menu.loginUser(SCANNER);
                        if (userService.loginUser(credentials) != null) {
                            validateUser = true;
                            user = userService.loginUser(credentials);
                        }
                        break;
                    case LOGOUT:
                        System.out.println(command.getCommandText());
                        validateUser = false;
                        user = null;
                        break;
                    case WRITE_ARTICLE:
                        System.out.println(command.getCommandText());
                        //write article logic
                        article = menu.writeArticle(SCANNER, user);
                        articleService.addArticle(article);
                        break;
                    case COMMENT:
                        System.out.println(command.getCommandText());
                        //comment article logic
                        articleService.showArticlesIdAndTitle();
                        articleId = menu.chooseArticleId(SCANNER);
                        article = articleService.returnArticleById(articleId);
                        feedbackService.showFeedbackForArticleId(articleId);
                        feedbackService.writeComment(SCANNER, article, user);
                        break;
                    case LIKE:
                        System.out.println(command.getCommandText());
                        //like article logic
                        articleService.showArticlesIdAndTitle();
                        articleId = menu.chooseArticleId(SCANNER);
                        articleService.likeArticle(articleId);
                        break;
                    case DISLIKE:
                        System.out.println(command.getCommandText());
                        //dislike article logic
                        articleService.showArticlesIdAndTitle();
                        articleId = menu.chooseArticleId(SCANNER);
                        articleService.dislikeArticle(articleId);
                        break;
                    case READ_ARTICLE:
                        System.out.println(command.getCommandText());
                        //read article logic
                        articleService.showArticlesIdAndTitle();
                        articleId = menu.chooseArticleId(SCANNER);
                        articleService.readArticle(articleId);
                        break;
                    case OPTION_NOT_DEFINED:
                    case EXIT:
                        System.out.println(command.getCommandText());
                        break;
                }
            } catch (InputMismatchException e) {
                System.err.println("Wrong data was inserted!");
            }
        }
        while (!AppCommands.EXIT.equals(command));
    }
}
