package service;

import dao.GenericDao;
import model.Article;
import model.Feedback;
import model.User;

import java.util.List;
import java.util.Scanner;

public class FeedbackService {

    private final GenericDao<Feedback> feedbackGenericDao = new GenericDao<>();
    private final Feedback feedback = new Feedback();

    public void showFeedbackForArticleId(long articleId) {
        List<Feedback> feedbacks = feedbackGenericDao.getAll(feedback);
        feedbacks.stream()
                .filter(feedback1 -> feedback1.getArticle().getId() == articleId)
                .forEach(feedback1 -> System.out.println(feedback1.getComments()));
    }

    public void writeComment(Scanner scanner, Article article, User user) {
        System.out.println("Write your comment (end character must be ~):");
        String comment = scanner.useDelimiter("~").next();
        scanner.reset();
        Feedback feedback = new Feedback(comment, user, article);
        feedbackGenericDao.add(feedback);
    }


}
