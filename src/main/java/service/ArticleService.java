package service;

import dao.GenericDao;
import model.Article;

import java.util.List;


public class ArticleService {

    private final GenericDao<Article> articleGenericDao = new GenericDao<>();
    private final Article article = new Article();

    public void addArticle(Article article) {
        articleGenericDao.add(article);
    }

    public void showArticlesIdAndTitle() {
        List<Article> articles = articleGenericDao.getAll(article);
        articles.forEach(article -> System.out.println("Id: " + article.getId()
                + " title: " + article.getTitle()));
    }

    public Article returnArticleById(long id) {
        return articleGenericDao.findById(article, id);
    }

    public void likeArticle(long articleId) {
        Article updatedArticle = articleGenericDao.findById(article, articleId);
        int likes = updatedArticle.getLikes() + 1;
        updatedArticle.setLikes(likes);
        articleGenericDao.update(updatedArticle);
    }

    public void dislikeArticle(long articleId) {
        Article updatedArticle = articleGenericDao.findById(article, articleId);
        int dislikes = updatedArticle.getDislikes() + 1;
        updatedArticle.setDislikes(dislikes);
        articleGenericDao.update(updatedArticle);
    }

    public void readArticle(long articleId) {
        Article readArticle = articleGenericDao.findById(article, articleId);
        System.out.println("Title: " + readArticle.getTitle());
        System.out.println("Category: " + readArticle.getCategory());
        System.out.println(readArticle.getContent());
    }
}
