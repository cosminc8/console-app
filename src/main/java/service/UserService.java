package service;

import dao.GenericDao;
import model.Article;
import model.User;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class UserService {

    private final GenericDao<User> userGenericDao = new GenericDao<>();

    public User loginUser(List<String> credentials) {
        String name = credentials.get(0);
        String pass = credentials.get(1);
        List<User> result = userGenericDao.getAll(new User());
        List<User> users = result.stream()
                .filter(user -> user.getEmail().equals(name) &&
                        user.getPassword().equals(pass))
                .collect(Collectors.toList());
        if (users.size() != 1) {
            System.out.println("Invalid username or password, try again!");
            return null;
        } else {
            System.out.println("Welcome " + users.get(0).getFirstName() + " " +
                    users.get(0).getLastName() + " id= "
                    + users.get(0).getId());
            return users.get(0);
        }
    }

    public void registerUser(User user) {
        userGenericDao.add(user);
    }


}
