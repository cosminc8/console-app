package view;

import model.Article;
import model.Category;
import model.Feedback;
import model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class Menu {
    public User showRegisterForm(Scanner scanner) {
        System.out.println("First Name: ");
        String firstName = scanner.nextLine();
        System.out.println("Last Name:");
        String lastName = scanner.nextLine();
        System.out.println("One line description of yourself:");
        String userDescription = scanner.nextLine();
        System.out.println("Email address:");
        String email = scanner.nextLine();
        System.out.println("Password for account:");
        String password = scanner.nextLine();

        return new User(firstName, lastName, email, password, userDescription,
                new HashSet<Article>(), new HashSet<Feedback>());
    }

    public List<String> loginUser(Scanner scanner) {
        System.out.println("Email address:");
        String email = scanner.nextLine();
        System.out.println("Password for account:");
        String password = scanner.nextLine();
        List<String> result = new ArrayList<>();
        result.add(email);
        result.add(password);
        return result;
    }

    public Article writeArticle(Scanner scanner, User user) {
        System.out.println("Choose category for your article:");
        for (Category category : Category.values()) {
            System.out.println(category);
        }
        Category category = Category.valueOf(scanner.nextLine());
        System.out.println("What is the title?");
        String title = scanner.nextLine();
        System.out.println("Write your article. End character must be ~");
        String articleText = scanner.useDelimiter("~").next();
        scanner.reset();

        return new Article(category, title, articleText, LocalDate.now(), 0, 0,
                user, new HashSet<Feedback>());
    }

    public long chooseArticleId(Scanner scanner) {
        System.out.println("Choose Id number for article.");
        String number = scanner.next();
        return Long.parseLong(number);
    }

}
