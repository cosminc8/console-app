package view;

import java.util.stream.Stream;

public enum AppCommands {
    REGISTER_USER("Please fill in this form:", false, true),
    LOGIN_USER("Please login into account:", false, true),
    LOGOUT("User has logged out from application.", true, false),

    WRITE_ARTICLE("Write an article", true, false),
    COMMENT("Add a comment for an article", true, false ),
    LIKE("Like the article", true, false),
    DISLIKE("Dislike the article", true, false),

    READ_ARTICLE("Read an article", true, true),

    OPTION_NOT_DEFINED("Please insert a valid option.", false, false),
    EXIT("Goodbye!", true, true);

    String commandText;
    boolean activeForLoggedInUser;
    boolean activeForNotLoggedInUser;

    AppCommands(String commandText, boolean activeForLoggedInUser, boolean activeForNotLoggedInUser) {
        this.commandText = commandText;
        this.activeForLoggedInUser = activeForLoggedInUser;
        this.activeForNotLoggedInUser = activeForNotLoggedInUser;
    }

    public String getCommandText(){
        return commandText;
    }

    public static void printOptions(boolean userLoggedIn){
        System.out.println("-------------------------------");
        System.out.println("Command line article app");
        System.out.println("-------------------------------");
        if (userLoggedIn){
            Stream.of(values())
                    .filter(appCommands -> appCommands.activeForLoggedInUser)
                    .forEach(appCommands -> System.out.println(appCommands.name()));
        }
        else {
            Stream.of(values())
                    .filter(appCommands -> appCommands.activeForNotLoggedInUser)
                    .forEach(appCommands -> System.out.println(appCommands.name()));
        }
        System.out.println("--------------------------------");
        System.out.println("Please insert an option:");
    }
}
